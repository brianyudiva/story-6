from django.shortcuts import render, redirect
from .models import Status
from .forms import Status_Form
# Create your views here.

def index(request):
    form = Status_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('/')

    response = {
        'forms' : form,
        'status' : Status.objects.all(),
    }
    return render(request, 'index.html', response)
