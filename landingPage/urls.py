from django.urls import path
from .views import index

app_name = 'landingPage'

urlpatterns = [
    path('', index, name='landing'),
]