from django import forms
from .models import Status

class Status_Form(forms.ModelForm):
    status = forms.CharField(widget=forms.TextInput())

    class Meta:
        model = Status
        fields = '__all__'