from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .apps import LandingpageConfig
from .views import index
from .models import Status

import time

# Create your tests here.

class LandingPageUnitTest(TestCase):

    def test_story6_app(self):
        self.assertEqual(LandingpageConfig.name, 'landingPage')
        self.assertEqual(apps.get_app_config('landingPage').name, 'landingPage')

    def test_landing_page_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)

    def test_landing_page_using_index_function(self):
        response = resolve('/')
        self.assertEqual(response.func, index)

    def test_landing_page_using_index_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, 'Halo, apa kabar?')

    def test_new_POST_request(self):
        response = Status.objects.create(status = 'check123')
        self.assertTrue(isinstance(response, Status))
        self.assertEqual(response.status, 'check123')

        # new_response = self.client.get('/')
        # html_response = new_response.content.decode('utf-8')
        # self.assertIn('test status', html_response)

class LandingPageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        # For local test
        # self.selenium = webdriver.Chrome('chromedriver.exe', chrome_options=chrome_options)  
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.get(self.live_server_url)
        super(LandingPageFunctionalTest, self).setUp()

    def test_input_post(self):
        selenium = self.selenium
        # find the form element
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')
        # Fill the form with data
        status.send_keys('Coba coba')
        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(2)

        self.assertIn('Coba coba', selenium.page_source)

    def tearDown(self):
        self.selenium.quit()
        super(LandingPageFunctionalTest, self).tearDown()

